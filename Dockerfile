FROM python:3.9

RUN apt-get update && apt-get install -y \
	wget \
	libhdf5-dev \
	python3-skimage \
	python3-skimage-lib \
	python3-numpy \
	python3-toml \
	python3-wrapt \
	python3-pyfftw \
	python3-h5py \
	jupyter-notebook \
	python3-pip \
	nodejs \
	npm \
	&& rm -rf /var/lib/apt/lists/*

ARG NB_USER=jovyan
ARG NB_UID=1000

RUN adduser --disabled-password --gecos "Jupyter User" --shell /bin/bash --uid $NB_UID $NB_USER
USER $NB_USER

RUN mkdir /home/$NB_USER/env
WORKDIR /home/$NB_USER/env

ENV PYVISTA_OFF_SCREEN=true \
	PYVISTA_USE_PANEL=true \
	PYVISTA_PLOT_THEME=document \
	PYVISTA_AUTO_CLOSE=false

SHELL ["sh", "-c"]

COPY --chown=$NB_USER . /home/$NB_USER/env
RUN git submodule update --init
RUN ./download_input_data.sh
# install newer version of matplotlib for interactive plots
RUN pip3 install --user matplotlib
RUN pip install --no-cache-dir notebook jupyterlab
RUN pip install --no-cache-dir jupyterhub

RUN pip install voila
RUN pip install ipywidgets
RUN pip install jupyterlab_widgets

RUN pip install ipympl
RUN pip install nodejs
RUN pip install npm
RUn pip install numpy==1.23.4

RUN export PATH="$HOME/.local/bin:$PATH"

CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0"]
