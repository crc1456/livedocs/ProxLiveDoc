#!/bin/bash

if [ ! -x "$TOOLBOX_PATH"/bart ] ; then
        echo "\$TOOLBOX_PATH is not set correctly!" >&2
        exit 1
fi
export PATH="$TOOLBOX_PATH":"$PATH"


set -euo pipefail

DATA_PATH=~/git/pyENLIVE/data/smallfov
bart ecalib -m2 -S $DATA_PATH/unders /dev/shm/tmp_ecal
bart normalize 8 /dev/shm/tmp_ecal /dev/shm/tmp_ecal_N

bart pattern $DATA_PATH/unders /dev/shm/tmp_pat


python3 <<HEREDOC
import cfl
import numpy as np

with open('smallfov_sens.npy', 'wb') as f:
	np.save(f, cfl.readcfl('/dev/shm/tmp_ecal_N'), allow_pickle=False)

with open('smallfov_ksp.npy', 'wb') as f:
	np.save(f, cfl.readcfl('$DATA_PATH/unders'), allow_pickle=False)

with open('smallfov_pat.npy', 'wb') as f:
	np.save(f, cfl.readcfl('/dev/shm/tmp_pat'), allow_pickle=False)
HEREDOC

