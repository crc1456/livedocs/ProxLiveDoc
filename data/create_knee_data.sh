#!/bin/bash

if [ ! -x "$TOOLBOX_PATH"/bart ] ; then
        echo "\$TOOLBOX_PATH is not set correctly!" >&2
        exit 1
fi
export PATH="$TOOLBOX_PATH":"$PATH"


set -euo pipefail

DATA_PATH=~/git/enlive/08_09_lowrank/knee/data/single_slice

US=1.420
SEED=20869

Y=$(bart show -d1 $DATA_PATH)
Z=$(bart show -d2 $DATA_PATH)

bart poisson -Y$Y -Z$Z -y$US -z$US -v -V05 -s$SEED -C12 /dev/shm/tmp_poisson_pat
bart fmac $DATA_PATH /dev/shm/tmp_poisson_pat /dev/shm/tmp_unders2

bart reshape 7 $Y $Z 1 /dev/shm/tmp_unders2 /dev/shm/tmp_unders

bart pattern /dev/shm/tmp_unders /dev/shm/tmp_pat


bart ecalib -m1 -S /dev/shm/tmp_unders /dev/shm/tmp_ecal
bart normalize 8 /dev/shm/tmp_ecal /dev/shm/tmp_ecal_N

python3 <<HEREDOC
import cfl
import numpy as np

with open('knee_sens.npy', 'wb') as f:
	np.save(f, cfl.readcfl('/dev/shm/tmp_ecal_N'), allow_pickle=False)

with open('knee_ksp.npy', 'wb') as f:
	np.save(f, cfl.readcfl('/dev/shm/tmp_unders'), allow_pickle=False)

with open('knee_pat.npy', 'wb') as f:
	np.save(f, cfl.readcfl('/dev/shm/tmp_pat'), allow_pickle=False)
HEREDOC


#rm /dev/shm/tmp_*.cfl /dev/shm/tmp_*.hdr
