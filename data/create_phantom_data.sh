#!/bin/bash

if [ ! -x "$TOOLBOX_PATH"/bart ] ; then
        echo "\$TOOLBOX_PATH is not set correctly!" >&2
        exit 1
fi
export PATH="$TOOLBOX_PATH":"$PATH"


set -euo pipefail
X=128
US=3

bart upat -Y $X -Z $X -y $US -z 1 pat_tmp 
bart phantom -x $X ph 
bart phantom -k -x $X -s 8 ph_k 
bart phantom -x $X -S 8 ph_sens 
bart normalize 8 ph_sens ph_sens_norm 
bart transpose 0 2 pat_tmp pat 
bart fmac pat ph_k ph_k_us_${US}


python3 <<HEREDOC
import cfl
import numpy as np

with open('phantom_sens.npy', 'wb') as f:
	np.save(f, cfl.readcfl('ph_sens_norm'), allow_pickle=False)

with open('phantom_ksp.npy', 'wb') as f:
	np.save(f, cfl.readcfl('ph_k_us_${US}'), allow_pickle=False)

with open('phantom_pat.npy', 'wb') as f:
	np.save(f, cfl.readcfl('pat'), allow_pickle=False)
HEREDOC

