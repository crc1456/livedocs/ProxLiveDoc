import sys
import time

sys.path.append("./ProxPython")

from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.experiments.experiment import Experiment
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.graphics import addColorbar


import numpy as np

import matplotlib.pyplot as plt


class P_ident(ProxOperator):
    def eval(self, u, prox_idx = None):
        return u


def centered_fft(x):
    return np.fft.fftshift(np.fft.fftn(np.fft.fftshift(x), axes=(0,1), norm='ortho'))

def centered_ifft(x):
    return np.fft.ifftshift(np.fft.ifftn(np.fft.ifftshift(x), axes=(0,1), norm='ortho'))


class P_sens(ProxOperator):
    """
    Projection sensitivities
    """

    def __init__(self, experiment):
        self.ncoils = experiment.ncoils
        self.nmaps = experiment.nmaps
        self.sens = experiment.A.reshape((*experiment.A.shape[0:2], self.ncoils, self.nmaps))

    def forw(self, im):
        # print("im: ", im.shape)
        ret = centered_fft(np.sum(im * self.sens, axis=(3,), keepdims=True))
        # print("forw: ", ret.shape)
        return ret.squeeze()

    def adj(self, k):
        #k = np.reshape(k, self.sens.shape)
        #print("k: ", k.shape)
        ret = np.sum(np.conj(self.sens) * centered_ifft(k[...,np.newaxis]), axis=(2,), keepdims=True)
        return ret.reshape((*ret.shape[0:2], 1, self.nmaps))

    def eval(self, u, prox_idx = None):
        #print("u: ", u.shape)
        ret = self.forw(self.adj(u))
        #print("ret: ", u.shape)
        return ret

class P_consis(ProxOperator):
    """
    Projection data conistency
    """

    def __init__(self, experiment):
        self.pat = experiment.pattern
        self.ksp = experiment.b

    def eval(self, u, prox_idx = None):
        return u - u * self.pat + self.ksp * self.pat


class MRI_Experiment(Experiment):
    """
    MRI SENSE experiment class
    """

    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name': 'MRI SENSE',
            'object': 'complex',
            'constraint': 'convex',
            'algorithm': 'AP',
            'MAXIT': 50,
            'TOL': -1e-6,
            'lambda_0': 0.85, # useful default for RAAR/DRl
            'lambda_max': 0.85,
            'data_ball': 1e-5,
            'diagnostic': False,
            'iterate_monitor_name': 'IterateMonitor',
            'rotate': False,
            'verbose': 0,
            'graphics': 1,
            'anim': True, # animate by default
            'debug': True
        }
        return defaultParams

    def __init__(self,
                 rescale = False,
                 dataname = 'phantom',
                 anim_slowdown = False,
                 **kwargs):
        """
        """
        # call parent's __init__ method
        super(MRI_Experiment, self).__init__(**kwargs)
        self.rescale = rescale
        self.dataname = dataname
        self.anim_slowdown = anim_slowdown

        # do here any data member initialization
        self.A = None
        self.b = None


    def loadData(self):
        """
        Load MRI dataset. Create the initial iterate.
        """

        def load_ksp_sens_pat(name):
            ksp = np.load('data/' + name + '_ksp.npy')
            sens = np.load('data/' + name + '_sens.npy')
            pat = np.load('data/' + name + '_pat.npy')
            return ksp, sens, pat

        # load data
        if self.dataname.startswith('phantom'):
            if not self.silent:
                print('Loading MRI Shepp-Logan data')
            ksp, sens, pat = load_ksp_sens_pat(self.dataname)
        elif self.dataname.startswith('smallfov'):
            if not self.silent:
                print('Loading MRI smallfov data')
            ksp, sens, pat = load_ksp_sens_pat(self.dataname)
        elif self.dataname.startswith('knee'):
            if not self.silent:
                print('Loading MRI knee data')
            ksp, sens, pat = load_ksp_sens_pat(self.dataname)
        elif self.dataname.startswith('head'):
            if not self.silent:
                print('Loading MRI head data')
            ksp, sens, pat = load_ksp_sens_pat(self.dataname)


        ksp = np.squeeze(ksp)
        sens = np.squeeze(sens)
        self.pattern = pat.squeeze()[...,np.newaxis]

        #print(ksp.shape)

        # Cannot work with 3D k-space:
        if len(ksp.shape) > 3:
            raise RuntimeError("Cannot handle 3D data for now")


        self.Nx = ksp.shape[0]
        self.Ny = ksp.shape[1]
        self.Nz = ksp.shape[2] # really N_coils

        self.ncoils = self.Nz
        self.nmaps = 1 if len(sens.shape) < 4 else sens.shape[3]

        self.A = sens.reshape(self.Nx, self.Ny, self.ncoils*self.nmaps)

        self.b = ksp

        self.norm_data = np.linalg.norm(self.b)

        self.u0 = np.zeros_like(self.b)
        #self.u0 = np.ones_like(self.b)



    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(MRI_Experiment, self).setupProxOperators()  # call parent's method

        self.proxOperators = []
        #self.productProxOperators = []

        self.proxOperators.append(P_consis)
        self.proxOperators.append(P_sens)
        # self.proxOperators.append(P_ident)

    def retrieveProxOperatorClasses(self):
        pass


    def u2im(self, u):
        tmp = centered_ifft(u)
        return np.sqrt(np.sum(np.abs(tmp)**2, axis=(2,), keepdims=True))

    def postprocess(self):
        self.solution = self.u2im(self.output['u'])

    def show(self):

        u_m = self.output['u_monitor']
        if isCell(u_m):
            u = u_m[0]
            u2 = u2 = u_m[len(u_m)-1]
        else:
            u = self.output['u']
            u2 = u_m

        if isCell(u):
            u = u[0]
        elif u.ndim == 2:
            u = u[:,0]


        u = self.u2im(u).squeeze()
        u2 = self.u2im(u2).squeeze()

        # figure(900)
        f, (ax1, ax2) = plt.subplots(2, 1,
                                                figsize=(self.figure_width, self.figure_height),
                                                dpi=self.figure_dpi)
        self.createImageSubFigure(f, ax1, u, 'best approx')

        changes = self.output['stats']['changes']
        time = self.output['stats']['time']
        time_str = "{:.{}f}".format(time, 5) # 5 is precision
        xLabel = "Iterations (time = " + time_str + " s)"
        algo_desc = self.algorithm.getDescription()
        title = "Algorithm " + algo_desc
        ax2.plot(changes)
        ax2.set_yscale('log')
        ax2.set_xlabel(xLabel)
        ax2.set_ylabel('log of iterate difference')
        ax2.set_title(title)

        plt.show()

    def animate(self, alg):
        """
        Display animation. This method is called
        after each iteration of the algorithm `alg` if
        data member `anim` is set to True.

        Parameters
        ----------
        alg : instance of Algorithm class
            Algorithm that is running.
        """
        # the output is a picture in a column vector
        if alg.u_new is not None:
            u = alg.u_new
        else:  # this the case when called before algorithm runs
            u = alg.u
        if isCell(u):
            u = u[0]
        elif u.ndim == 2:
            u = u[:,0]

        if self.anim_slowdown:
            time.sleep(0.1)
        image = self.u2im(u).squeeze()
        title = "Iteration " + str(alg.iter)
        self.animateFigure(image, title)


    def createImageSubFigure(self, f, ax, u, title = None):
        im = ax.imshow(u, cmap='gray')
        addColorbar(im)
        if title is not None:
            ax.set_title(title)


if __name__ == '__main__':
    MAXIT=40
    # data='phantom'
    data='smallfov'
    # data='knee'
    # data='head'
    MRI_RAAR = MRI_Experiment(algorithm='RAAR', MAXIT=MAXIT, dataname=data, anim=True, anim_step=1)
    MRI_RAAR.run()

    MRI_AP = MRI_Experiment(algorithm='AP', MAXIT=MAXIT, dataname=data, anim=False, anim_step=1)
    MRI_AP.run()

    MRI_RAAR.show()
    MRI_AP.show()

    diff = np.abs(MRI_RAAR.solution - MRI_AP.solution).squeeze()
    nrmse = np.sum(np.abs(diff)**2)/np.sum(np.abs(MRI_RAAR.solution)**2)
    print('NRMSE: ', nrmse)

    f, ax = plt.subplots(1,1)
    MRI_RAAR.createImageSubFigure(f, ax, diff, "diff")
    plt.show()



    try:
        import cfl
        cfl.writecfl('MRIout', MRI_RAAR.solution)
    except:
        pass
