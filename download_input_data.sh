#!/bin/bash
set -euo pipefail

cd ProxPython/InputData

mkdir -p Phase || true
wget -nc -q 'https://vaopt.math.uni-goettingen.de/data/Phase.tar.gz'
(
cd Phase
tar xzf ../Phase.tar.gz
)

mkdir -p CT || true
wget -nc -q 'https://vaopt.math.uni-goettingen.de/data/CT.tar.gz'
(
cd CT
tar xzf ../CT.tar.gz
)

## Somehow missing from the server
#mkdir -p OrbitalTomog || true
#wget -nc -q 'https://vaopt.math.uni-goettingen.de/data/OrbitalTomog.tar.gz'
#(
#cd OrbitalTomog
#tar xzf ../OrbitalTomog.tar.gz
#)
