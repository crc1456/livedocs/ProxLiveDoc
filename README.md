#### Private CRC 1456 Binderhub:<br>
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2FProxLiveDoc/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2FProxLiveDoc/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://livedocs-private.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2FProxLiveDoc/HEAD?urlpath=voila)<br>

#### Public CRC 1456 Binderhub:<br>
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-Jupyterlab-orange)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2FProxLiveDoc/HEAD)
[![Binderhub](https://img.shields.io/badge/CRC1456%20Binderhub-JupyterClassic-orange)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2FProxLiveDoc/HEAD?urlpath=tree)
[![Voila](https://img.shields.io/badge/CRC1456%20Binderhub-Voila-green)](http://livedocs.math.uni-goettingen.de:30901/v2/gwdg/crc1456%2Flivedocs%2FProxLiveDoc/HEAD?urlpath=voila)<br>

#### Other LiveDocs:<br>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fcrc1456%2Flivedocs%2FProxLiveDoc.git/HEAD)
[![lite-badge](https://img.shields.io/badge/CRC1456-Jupyterlite-yellow)](https://crc1456.pages.gwdg.de/livedocs/ProxLiveDoc/lab/index.html)
[![Docker](https://img.shields.io/badge/CRC1456-Dockerhub-blue)](https://gitlab.gwdg.de/crc1456/livedocs/ProxLiveDoc/container_registry)<br>


<!--#### Static HTMLs (with limited functionality):<br>
[![static-html](https://img.shields.io/badge/HTML-ART_AP-white)](https://crc1456.pages.gwdg.de/livedocs/ProxLiveDoc/files/ART_AP.html)
[![static-html](https://img.shields.io/badge/HTML-JWST_ADMM_DRl-white)](https://crc1456.pages.gwdg.de/livedocs/ProxLiveDoc/files/JWST_ADMM_DRl.html)
[![static-html](https://img.shields.io/badge/HTML-MRI_phantom-white)](https://crc1456.pages.gwdg.de/livedocs/ProxLiveDoc/files/MRI_phantom.html)
[![static-html](https://img.shields.io/badge/HTML-MRI_smallfov-white)](https://crc1456.pages.gwdg.de/livedocs/ProxLiveDoc/files/MRI_smallfov.html)
[![static-html](https://img.shields.io/badge/HTML-orbit_planar_DRl-white)](https://crc1456.pages.gwdg.de/livedocs/ProxLiveDoc/files/orbit_planar_DRl.html)<br>
-->

**************

# ProxToolbox LiveDoc

## ProxToolbox

The ProxToolbox is a collection of modules for solving mathematical problems using fixed point iterations with proximal operators. It was used to generate many if not all of the numerical experiments conducted in the papers in the ProxMatlab Literature folder. 

For a complete listing of papers with links, go to the [groups publications](https://vaopt.math.uni-goettingen.de/en/publications.php).

For more reference on the toolbox, please refer to https://gitlab.gwdg.de/nam/ProxPython.

## This repository

In this repository you will find showcases for the ProxToolbox library in Jupyter notebooks. These showcases implement some of the available examples for the ProxToolbox contained in the [experiments](https://gitlab.gwdg.de/nam/ProxPython/-/tree/master/proxtoolbox/experiments) folder.

- [`00_MRI_step-by-step.ipynb`](https://gitlab.gwdg.de/crc1456/livedocs/ProxLiveDoc/-/blob/master/00_MRI_step-by-step.ipynb): A Jupyter notebook exploring the application of ProxToolbox in Magnetic Resonance Imaging (MRI)
- [`ART_AP.ipynb`](https://gitlab.gwdg.de/crc1456/livedocs/ProxLiveDoc/-/blob/master/ART_AP.ipynb): A Jupyter notebook exploring the ART Experiment in a phantom MRI data. Note: this application is not suitable for BinderHub.
- [`JWST_ADMM_DRl.ipynb`](https://gitlab.gwdg.de/crc1456/livedocs/ProxLiveDoc/-/blob/master/JWST_ADMM_DRl.ipynb): A Jupyter notebook exploring the ADMM algorithm in the JWST experiment.
- [`MRI_phantom.ipynb`](https://gitlab.gwdg.de/crc1456/livedocs/ProxLiveDoc/-/blob/master/MRI_phantom.ipynb): A Jupyter nootebook exploring the usage of ProxToolbox in MRI Phantom data.
- [`MRI_smallfov.ipynb`](https://gitlab.gwdg.de/crc1456/livedocs/ProxLiveDoc/-/blob/master/MRI_smallfov.ipynb): A Jupyter notebook exploring the usage of ProxToolbox in MRI smallfov data.
- [`Sudoku_DRl.ipynb`](https://gitlab.gwdg.de/crc1456/livedocs/ProxLiveDoc/-/blob/master/Sudoku_DRl.ipynb): A Jupyter notebook implementing a Sudoku solver using ProxToolbox.


<!-- 
- Build Docker container: `docker build -t proxlivedoc .`
- Run: `docker run --name bart --publish 8888:8888 -it --rm --init proxlivedoc`, then copy Jupyter URL including token to browser


Or, to run it without docker:

* Fetch the required version of the ProxToolbox with `git submodule update --init`
* Fetch the needed raw data with `./download_input_data`
* Use `jupyter notebook` to get the notebooks running.

Note that, if you use this Livedoc in this way, *you* will need to ensure that all dependeces are installed.
-->